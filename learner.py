import os
import glob
import textBlobTester
from random import shuffle
from collections import Counter
from cleaner import Cleaner
from sklearn import svm
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, confusion_matrix

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import Normalizer

import pylab as pl

class Comment():
    """ Simple class to hold comment data
    words is the cleaned, filtered words of the comment
    wordsUnfiltered is the unmodified words of the comment
    wordsAsIndex are the words converted to their indices into the allWords file
    features are the words converted into a feature vector for training
        The feature vector is simply a vector of boolean if the word at that index occurs or not
    label is the label of the comment (positive, negative, neutral)
    file is the file that the comment is located in
    """
    __slots__ = ('words', 'wordsUnfiltered', 'wordsAsIndex', 'features', 'label', 'file')

    def __init__(self):
        pass

class DataBuilder():
    """ A class to make various kinds of data for training and testing """
    def __init__(self):
        self.files = glob.glob(os.path.join('commentData', '*.done.txt'))
        self.allWordsFile = os.path.join('commentData', 'allWords.txt')
        self.cleaner = Cleaner()
        self.buildWordList()
        self.buildWordIndexDictFromWordList()

    def buildWordList(self):
        """ Write to a file all the words in the dataset, ordered by most to least common """
        counter = Counter()
        for directory in self.files:
            with open(directory) as f:
                next(f) # skip the first line with header info
                for line in f:
                    if(len(line.strip())) > 0:
                        label = line.strip()[-1]
                        if label != "s": # skip any comments marked skip
                            words = self.getCleanWords(line)
                            counter.update(words)
        with open(self.allWordsFile, 'w') as f:
            for word in counter.most_common():
                f.write((word[0] + "\n").encode('UTF-8'))

    def buildComments(self):
        """ Construct comments from the given data in *.done.txt files
        returns a list of all labeled comments """
        comments = []
        for directory in self.files:
            with open(directory) as f:
                next(f) # skip the first line with header info
                for line in f:
                    if(len(line.strip())) > 0:
                        comment = Comment()
                        comment.wordsUnfiltered = line.strip().split("\t")[0]
                        comment.words = self.getCleanWords(line)
                        comment.wordsAsIndex = self.processComment(comment.words)
                        comment.features = self.getCommentFeatures(comment.wordsAsIndex)
                        comment.file = directory
                        label = line.strip()[-1]
                        if label == "-":
                            label = 1
                        elif label == "+":
                            label = 2
                        elif label == "n":
                            label = 3
                        comment.label = label
                        comments.append(comment)
        return comments

    def buildWordIndexDictFromWordList(self):
        """ Reads the list of all words in the dataset and constructs a wordIndexDict
        where key is the word, and value is the index of that word in the file """
        words = {}
        count = 0
        try:
            with open(self.allWordsFile) as f:
                for line in f:
                    line = line.strip()
                    words[line] = count
                    count += 1
        except IOError:
            # The word list doesn't exist, so build it first
            self.buildWordList()
            with open(self.allWordsFile) as f:
                for line in f:
                    line = line.strip()
                    words[line] = count
                    count += 1
        self.wordIndexDict = words

    def convertTextCommentToFeatures(self, text):
        """ Converts given text into a feature vector. Used in predicting class of new data """
        words = text.strip()
        words = self.getCleanWords(words)
        wordsAsIndex = self.processComment(words)
        features = self.getCommentFeatures(wordsAsIndex)
        return features

    def processComment(self, comment):
        """ Return a list of index values mapping each word in the comment to its index in the
        list of all words """
        return [self.wordIndexDict.get(word) for word in comment if self.wordIndexDict.get(word) is not None]

    def getCommentFeatures(self, commentAsIndex):
        """ Given a comment converted to words as indices of the allWords file,
        return the feature vector """
        features = [0]*len(self.wordIndexDict)
        for index in commentAsIndex:
            features[index] = 1
        return features

    def getCleanWords(self, line):
        """ Process the given line (comment) by replacing urls, emails, slang, and emoticons,
        tokenizing the line, removing punctuation, and removing too-small, too-long, and stop words.
        """
        # This order is important because without replacing URLs before emoticons
        # http://... will get have the :/ part replaced with an emoticon phrase
        line = self.cleaner.replaceURLs(line)
        line = self.cleaner.replaceEmail(line)
        line = self.cleaner.replaceSlang(line)
        line = self.cleaner.replaceEmoticons(line)
        tokens = self.cleaner.tokenize(line)
        tokens = [self.cleaner.removePunctuation(token.decode('UTF-8')) for token in tokens]
        words = self.cleaner.getValidWords(tokens)
        return words

class Learner():
    """ Class to do machine learning training and prediction 
    comments is a list of labeled comments to train on, shuffled to remove bias
    unshuffledcomments are the comments unshuffled 
    trainers is a list of different types of machine learning algorithms with different characteristics
        linear SVMs, non-linear SVMs, Naive-Bayes are all tried
    """
    __slots__ = ('comments', 'clf')
    def __init__(self, comments):
        self.comments = comments
        self.unshuffledComments = comments
        shuffle(self.comments) # shuffle to randomize the data to avoid patterns
        self.trainers = [svm.LinearSVC(C=.1), svm.LinearSVC(C=1), svm.LinearSVC(C=10), svm.SVC(C=.1), svm.SVC(C=1), svm.SVC(C=10), GaussianNB()]

    def train(self, bestIndex=None, percentToTrainOn=.7):
        """ Train on given percent of data (default 70%) 
        if bestIndex is give, uses only the trainer at the index given, otherwise trains all trainers """
        nonSkippedComments = [comment.features for comment in self.comments if comment.label != "s"]
        nonSkippedLabels = [comment.label for comment in self.comments if comment.label != "s"]
        # Train on 70% of the available data
        trainX = nonSkippedComments[:int(len(nonSkippedComments) * percentToTrainOn)]
        trainY = nonSkippedLabels[:int(len(nonSkippedLabels) * percentToTrainOn)]

        print("train")
        print("len total: " + str(len(nonSkippedComments)) + " len train: " + str(len(trainX)))

        c = Counter(trainY)
        print(c)
        if(bestIndex is None):
            for trainer in self.trainers:
                trainer.fit(trainX, trainY)
        else:
            trainer = self.trainers[bestIndex]
            trainer.fit(trainX, trainY)

    def validate(self):
        """ Find the best trainer using 10% 
        of the data (so it is all unseen data) 
        Return the index of the best trainer """
        nonSkippedComments = [comment.features for comment in self.comments if comment.label != "s"]
        nonSkippedLabels = [comment.label for comment in self.comments if comment.label != "s"]

        validateX = nonSkippedComments[int(len(nonSkippedComments) * .7):int(len(nonSkippedComments) * .8)]
        validateY = nonSkippedLabels[int(len(nonSkippedLabels) * .7):int(len(nonSkippedLabels) * .8)]

        print("validation")
        print("len total: " + str(len(nonSkippedComments)) + " len validate: " + str(len(validateX)))

        best = 0
        bestTrainer = None
        for trainer in self.trainers:
            correct = 0
            predict = trainer.predict(validateX)
            for i in range(len(predict)):
                if predict[i] == validateY[i]:
                    correct += 1
            percentCorrect = 1.0*correct/len(validateY)
            if percentCorrect > best:
                best = percentCorrect
                bestTrainer = trainer
            print("Num correct: " + str(correct) + ", percent: " + str(1.0*correct/len(validateY)))
        return bestTrainer, self.trainers.index(bestTrainer)


    def test(self, trainer):
        """ Test a given trainer 1 time on 30%
        of the data (all unseen data) 
        return accuracy score and confusion matrix """
        nonSkippedComments = [comment.features for comment in self.comments if comment.label != "s"]
        nonSkippedLabels = [comment.label for comment in self.comments if comment.label != "s"]
        testX = nonSkippedComments[int(len(nonSkippedComments) * .7):]
        testY = nonSkippedLabels[int(len(nonSkippedLabels) * .7):]

        print("test")
        print("len total: " + str(len(nonSkippedComments)) + " len test: " + str(len(testX)))

        predict = trainer.predict(testX)
        accuracyScore = accuracy_score(testY, predict)
        confusionMatrix = confusion_matrix(testY, predict)

        return accuracyScore, confusionMatrix

    def readFromFilesForSciKit(self, files, XWords, Y):
        """ Read from the labeled comment data files and add words (unfiltered)
        and their labels. Done for using built-in scikit feature extractors """
        for directory in files:
            with open(directory) as f:
                next(f) # skip the first line with header info
                for line in f:
                    if(len(line.strip())) > 0:
                        label = line.strip()[-1]
                        words = line.strip()[:-1]
                        if label != "s":
                            XWords.append(words)
                            if label == "-":
                                label = 1
                            elif label == "+":
                                label = 2
                            elif label == "n":
                                label = 3
                            Y.append(label)

    def testSciKitsFunctions(self, dataBuilder):
        """ Test scikit's built-in feature extractors and dimensionality reduction techniques
        Prints accuracy score and confusion matrix before and after dimensionality reduction """
        vectorizer = TfidfVectorizer(max_df=0.5, max_features=893, stop_words='english')

        trainY = []
        trainXWords = []
        testY = []
        testXWords = []

        # Read in the first 10 files for testing and the rest for training
        self.readFromFilesForSciKit(dataBuilder.files[10:], trainXWords, trainY)
        self.readFromFilesForSciKit(dataBuilder.files[:10], testXWords, testY)
        trainX = vectorizer.fit_transform(trainXWords)
        testX = vectorizer.fit_transform(testXWords)

        # Try training and testing using all features
        trainer = svm.LinearSVC(C=.1)
        trainer.fit(trainX, trainY)
        print("Accuracy with no dimensionality reduction: " + str(accuracy_score(testY, trainer.predict(testX))))

        # Do some dimensionality reduction to 100 features
        # Train and test again
        lsa = TruncatedSVD(100)
        trainX = lsa.fit_transform(trainX)
        trainX = Normalizer(copy=False).fit_transform(trainX)
        testX = lsa.fit_transform(testX)
        testX = Normalizer(copy=False).fit_transform(testX)
        trainer = svm.LinearSVC(C=.1)
        trainer.fit(trainX, trainY)
        print("Accuracy with dimensionality reduction to 100: " + str(accuracy_score(testY, trainer.predict(testX))))

def determineBestTrainer(comments):
    """ Using validate(), validate 10 times and return average best trainer """
    c = Counter()
    for i in range(10):
        learner = Learner(comments)
        learner.train()
        bestTrainer, bestIndex = learner.validate()
        c.update([bestIndex])
    bestIndex = int(c.most_common(1)[0][0])
    return learner.trainers[bestIndex], bestIndex

def determineAverageAccuracy(comments, bestIndex):
    """ Using test(), test the best trainer 100 times and return the 
    average accuracy score and average confusion matrix """ 
    tot = 0
    avgConfusionMatrix = None
    for i in range(100):
        learner = Learner(comments)
        learner.train(bestIndex)
        percentCorrect, confusionMatrix = learner.test(learner.trainers[bestIndex])
        tot += percentCorrect
        if avgConfusionMatrix is None:
            avgConfusionMatrix = confusionMatrix
        else:
            avgConfusionMatrix += confusionMatrix
    return tot/100, avgConfusionMatrix/100

def testOurMethodFully(comments):
    """ Find the best trainer, find the average accuracy, report average accuracy
    print confusion matrix graphically and on command line """
    bestTrainer, bestIndex = determineBestTrainer(comments)
    print("best index: " + str(bestIndex))
    avgAccuracy, confusionMatrix = determineAverageAccuracy(comments, bestIndex)
    print("Using best trainer, average percent: " + str(avgAccuracy))
    print("Confusion matrix: " + str(confusionMatrix))
    pl.matshow(confusionMatrix, cmap=pl.cm.gray)
    pl.title('Confusion matrix')
    pl.colorbar()
    pl.ylabel('True label')
    pl.xlabel('Predicted label')
    pl.show()

def testOurMethodGivenTrainerIndex(comments, index):
    """ Finds the average accuracy of a given trainer, report average accuracy
    print confusion matrix graphically and on command line """
    avgAccuracy, confusionMatrix = determineAverageAccuracy(comments, index)
    print("Using best trainer, average percent: " + str(avgAccuracy))
    print("Confusion matrix: " + str(confusionMatrix))
    pl.matshow(confusionMatrix, cmap=pl.cm.gray)
    pl.title('Confusion matrix')
    pl.colorbar()
    pl.ylabel('True label')
    pl.xlabel('Predicted label')
    pl.show()

def testOurMethodOnce(comments):
    """ Just test our trained method one using the first SVM in trainers """
    learner = Learner(comments)
    learner.train(0)
    percentCorrect = learner.test(learner.trainers[0])
    print("percent correct: " + str(percentCorrect))

def testSciKitsMethods(comments, dataBuilder):
    """ Test scikits feature extraction methods """
    learner = Learner(comments)
    learner.testSciKitsFunctions(dataBuilder)

def testTextBlobsMethods(comments):
    """ Test TextBlob's sentiment analyzer, print accuracy score and confusion matrix """
    labels = []
    predictionsClean = []
    predictionsUnfiltered = []
    for comment in comments:
        label = comment.label
        if label != "s":
            predictCleaned = textBlobTester.getSentiment(" ".join(comment.words))
            predictUnfiltered = textBlobTester.getSentiment(comment.wordsUnfiltered.decode('UTF-8'))
            predictionsClean.append(predictCleaned)
            predictionsUnfiltered.append(predictUnfiltered)
            labels.append(label)

    accuracyScoreClean = accuracy_score(labels, predictionsClean)
    confusionMatrixClean = confusion_matrix(labels, predictionsClean)

    accuracyScoreUnfiltered = accuracy_score(labels, predictionsUnfiltered)
    confusionMatrixUnfiltered = confusion_matrix(labels, predictionsUnfiltered)

    print("TextBlob accuracy with our cleaned words: " + str(accuracyScoreClean))
    print("TextBlob confusion matrix with our cleaned words: " + str(confusionMatrixClean))
    pl.matshow(confusionMatrixClean, cmap=pl.cm.gray)
    pl.title('Confusion matrix')
    pl.colorbar()
    pl.ylabel('True label')
    pl.xlabel('Predicted label')
    pl.show()

    print("TextBlob accuracy with unfiltered words: " + str(accuracyScoreUnfiltered))
    print("TextBlob confusion matrix with unfiltered words: " + str(confusionMatrixUnfiltered))
    pl.matshow(confusionMatrixUnfiltered, cmap=pl.cm.gray)
    pl.title('Confusion matrix')
    pl.colorbar()
    pl.ylabel('True label')
    pl.xlabel('Predicted label')
    pl.show()

def testSingleMadeUpData(comments, dataBuilder, madeUpComment):
    """ Given a made-up or single comment (text), print the prediction of its class """
    madeUpComment = dataBuilder.getCleanWords(madeUpComment)
    wordsAsIndex = dataBuilder.processComment(madeUpComment)
    features = dataBuilder.getCommentFeatures(wordsAsIndex)
    learner = Learner(comments)
    learner.train(0)
    result = learner.trainers[0].predict(features)
    if result[0] == 1:
        print("Negative sentiment :(")
    elif result[0] == 2:
        print("Positive sentiment :)")
    else:
        print("Neutral sentiment :|")

def test():
    dataBuilder = DataBuilder()
    comments = dataBuilder.buildComments()

    # uncomment to get best trainer and average accuracy
    testOurMethodFully(comments)

    # uncomment to get average accuracy of specific trainer
    #testOurMethodGivenTrainerIndex(comments, 0) # linSVM C=.1
    #testOurMethodGivenTrainerIndex(comments, 1) # linSVM C=1
    #testOurMethodGivenTrainerIndex(comments, 2) # linSVM C=10
    #testOurMethodGivenTrainerIndex(comments, 6) # Gaussian Naive-Bayes
    ''' warning these are slow and innacurate, you probably don't want to run them '''
    #testOurMethodGivenTrainerIndex(comments, 3) # SVM C=.1
    #testOurMethodGivenTrainerIndex(comments, 4) # SVM C=1
    #testOurMethodGivenTrainerIndex(comments, 5) # SVM C=10
    ''' end warning '''

    # uncomment to get sci kit's average accuracy
    #testSciKitsMethods(comments, dataBuilder)

    # uncomment to get text blob's average accuracy
    #testTextBlobsMethods(comments)

    # uncomment to get a single made up data test
    #test = "It was okay I guess. Meh."
    #testSingleMadeUpData(comments, dataBuilder, test)

    # uncomment to get a single full test
    #testOurMethodOnce(comments)

def main():
    test()


if __name__ == '__main__':
    main()

