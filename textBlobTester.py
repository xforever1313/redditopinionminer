""" To install textblob:
pip install -U textblob
python -m textblob.download_corpora lite
"""

from textblob import TextBlob

def getSentiment(words):
    """ Convert the textblob sentiment to a 3-class sentiment
    of negative, positive, or neutral """
    words = TextBlob(words)
    sentiment = words.sentiment
    if -1 <= sentiment.polarity <= -.01:
        return 1 # negative
    elif .01 <= sentiment.polarity <= 1:
        return 2 # positive
    else:
        return 3 # neutral

def getSubjectivity(words):
    """ Return the subjectivity of a sentence
    1.0 is very subjective, 0 is very objective """
    words = TextBlob(words)
    return words.sentiment.subjectivity