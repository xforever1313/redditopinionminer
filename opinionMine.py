from collections import Counter
from cleaner import Cleaner
from redditData import redditSubmissionScraper
from learner import DataBuilder, Learner
import argparse
import re

def getKeywords(text):
    """ Return the keywords in a given text
    Based off of code here: https://gist.github.com/alexbowe/879414
    Inspired by paper: http://sunamkim.me/documents/coling2010.pdf
    """
    cleaner = Cleaner()
    tokens = cleaner.tokenize(text)
    cleanedTokens = []
    for token in tokens:
        try:
            token = token.decode('UTF-8')
            token = cleaner.removePunctuation(token)
            cleanedTokens.append(token)
        except UnicodeDecodeError:
            token = ""
    posTokens = cleaner.posTagTokens(cleanedTokens)
    tree = cleaner.getTree(posTokens)
    terms = cleaner.getValidTerms(tree)
    keywords = []
    for term in terms:
        keywords.append(" ".join(term))
    return keywords

def writeCommentData(scraper, postID):
    """ Writes the comments from a given Reddit postID to a text file """
    topSubmissionComments = scraper.getTopSubmissionComments()
    topComments = scraper.getTopComments(topSubmissionComments)
    with open("commentData/" + postID + ".txt", "w") as f:
        f.write("comment\tsentiment\n")
        for comment in topComments.keys():
            commentBody = re.sub("\n", " ", comment[0].body)
            commentBody = re.sub("\t", "    ", commentBody).encode('UTF-8')
            f.write(commentBody + "\t" + "\n")
            for reply in topComments[comment]:
                commentBody = re.sub("\n", " ", reply.body)
                commentBody = re.sub("\t", "    ", commentBody).encode('UTF-8')
                f.write(commentBody + "\n")

def writeAllCommentData(postIDFile):
    """ Write comments in all given postIDs to text files """ 
    with open(postIDFile) as f:
        for line in f:
            line = line.strip()
            scraper = redditSubmissionScraper(line)
            writeCommentData(scraper, line)

def getCommentFeatures(comment, dataBuilder):
    """ Returns the feature vector of a given comment """
    commentBody = re.sub("\n", " ", comment.body)
    commentBody = re.sub("\t", "    ", commentBody).encode('UTF-8')
    features = dataBuilder.convertTextCommentToFeatures(commentBody)
    return features

def analyzeCommentData(scraper, learner, dataBuilder):
    """ Takes a scraper which has been given a post to analyze, 
    a trained learner, and a dataBuilder and returns the scores
    of the Reddit post.
    Scores are calculated as the sum of the upvotes per predicted class.
    All upvotes of a comment predicted as a given class go towards the 
    score of the class """
    topSubmissionComments = scraper.getTopSubmissionComments()
    topComments = scraper.getTopComments(topSubmissionComments)
    scores = {1 : 0, 2 : 0, 3 : 0}
    for comment in topComments.keys():
        features = getCommentFeatures(comment[0], dataBuilder)
        prediction = learner.trainers[0].predict(features)[0]
        ups = comment[0].ups
        score = scores.get(prediction)
        score += ups
        scores[prediction] = score
        for reply in topComments[comment]:
            features = getCommentFeatures(reply, dataBuilder)
            prediction = learner.trainers[0].predict(features)[0]
            ups = reply.ups
            score = scores.get(prediction)
            score += ups
            scores[prediction] = score
    return scores

def getValidKeywords(submissionData):
    """ Given data from getSubmittedData (the self-post text or link text)
    returns good keywords. Good keywords are keyword phrases or single keywords
    that appear at least twice and are longer than 2 characters """
    if submissionData.get('type') == "self":
        text = submissionData.get('text')
        title = submissionData.get('title')
        keywords = []
        if text is not None:
            keywords = getKeywords(text)
        if title is not None:
            keywords.extend(getKeywords(title))
    elif submissionData.get('type') == 'link':
        text = submissionData.get('text')
        title = submissionData.get('title')
        descr = submissionData['descr']
        keywords = submissionData['keywords']
        if keywords is None or len(keywords) <= 0:
            keywords = []
        if text is not None:
            keywords.extend(getKeywords(text))
        if title is not None:
            keywords.extend(getKeywords(title))
        if descr is not None:
            keywords.extend(getKeywords(descr))
    c = Counter(keywords)
    validKeywords = []
    for keywordData in c.most_common():
        count = keywordData[1]
        keywords = keywordData[0].split(" ")
        # Only select keywords that are phrases or single words that appear at least twice
        if len(keywords) > 1 or (len(keywords) == 1 and len(keywords[0]) > 2 and count > 2):
            validKeywords.append(" ".join(keywords))
    return validKeywords
        
def analyzeAllCommentData(postIDFile, learner, dataBuilder):
    """ Find the score and sentiment of all the Reddit posts given in the postID file.
    Prints results to the command line """
    with open(postIDFile) as f:
        for line in f:
            line = line.strip()
            scraper = redditSubmissionScraper(line)
            submissionData = scraper.getSubmittedData()
            keywords = getValidKeywords(submissionData)
            scores = analyzeCommentData(scraper, learner, dataBuilder)
            printResults(line, submissionData.get('title'), keywords, scores)

def printResults(postID, title, keywords, scores):
    """ Print the results of an analyzed post """
    print("------------------------------------------------------------")
    print("Results for Reddit post ID: " + postID)
    print("------------------------------------------------------------")
    print("Title: " + str(title))
    print("Keywords: ")
    for keyword in keywords:
        print("\t" + keyword)
    print("------------------------------------------------------------")
    print("Sentiment Scores: Negative = " + str(scores[1]) + " Positive = " + str(scores[2]) + " Neutral = " + str(scores[3]))
    maxType = max(scores, key=scores.get)
    if maxType == 1:
        sentiment = "negative :("
    elif maxType == 2:
        sentiment = "positive :)"
    else:
        sentiment = "neutral :|"
    print("Overall Sentiment is " + sentiment)
    print("------------------------------------------------------------\n")

def trainLearner(dataBuilder):
    """ Train a learner """
    comments = dataBuilder.buildComments()
    learner = Learner(comments)
    # Train with the linear SVM C=.1, use all available data
    learner.train(0, 1)
    return learner       
                
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("postIDFile", type=str, help="The file containing reddit posts to get comment data from.")
    parser.add_argument("--scrape", dest='scrape', action='store_true', help="Add this option to scrape from the reddit ids in the postIDFile and save the comment data in a file rather than analyze them.")
    args = parser.parse_args()
    if args.scrape:
        writeAllCommentData(args.postIDFile)
    else:
        dataBuilder = DataBuilder()
        learner = trainLearner(dataBuilder)
        analyzeAllCommentData(args.postIDFile, learner, dataBuilder)

if __name__ == '__main__':
    main()