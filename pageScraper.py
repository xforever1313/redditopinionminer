from goose import Goose

def test(url):
    """ test the functionality by printing our relevant information from the page """
    title, descr, text, keywords = getPageInfo(url)
    print(title)
    print(descr)
    print(text)
    print(keywords)

def getPageInfo(url):
    """ Get information from a given url 
    returns a 4-tuple of title, description, text, keywords.
    If any are unavailable, that part of the tuple is None """
    g = Goose()
    try:
        article = g.extract(url=url)
        return article.title.encode('UTF-8'), article.meta_description.encode('UTF-8'), article.cleaned_text.encode('UTF-8'), article.meta_keywords.encode('UTF-8')
    except:
        return None, None, None, None

if __name__ == '__main__':
    test("http://earthquake.usgs.gov/earthquakes/eventpage/usb000pq41#summary")
    test("https://en.wikipedia.org/wiki/Great_Fire_of_Meireki")