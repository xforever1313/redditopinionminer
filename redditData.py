import praw
from pageScraper import getPageInfo

class redditSubmissionScraper():
    """ A Class to gather data from Reddit """

    def __init__(self, submissionId):
        self.r = praw.Reddit(user_agent='RedditOpinionMiner by /u/VoidXC and /u/xforever1313')
        # This config change converts &amp; to just & in url links so that the url is valid in getPageInfo
        self.r.config.decode_html_entities = True
        self.submissionId = submissionId
        self.submission = self.r.get_submission(submission_id=self.submissionId, comment_limit=30, comment_sort='top')

    def getTopSubmissionComments(self, topLimit=15):
        """ Returns the top n top-level comments of the reddit post (submission) where n = topLimit """
        comments = self.submission.comments # TRY comments_by_id also!!!!!!!!!!!!!!!!!!!!
        commentsLst = []
        for comment in comments:
            try:
                score = comment.ups
                commentsLst.append((comment, score))
            except AttributeError:
                pass
        sortedCommentsByScore = sorted(commentsLst, key=lambda tup: (-tup[1], tup[0]))
        return sortedCommentsByScore[:topLimit]
        
    def getTopComments(self, topSubmissionComments):
        """ Gathers the top 3 replies to a top-level comment given by getTopSubmissionComments
        and returns both top-level comments and replies in a dictionary where the key is a tuple
        of the top-level comment and its score, and the value is a list of replies to it """
        topComments = {}
        replyLimit = 3
        commentIndex = 0
        for topSubmissionComment in topSubmissionComments:
            replies = topSubmissionComment[commentIndex]._replies[:replyLimit]
            for reply in replies:
                if type(reply) == praw.objects.MoreComments:
                    replies.remove(reply)
            topComments[topSubmissionComment] = replies
        return topComments

    def getSubmittedData(self):
        """ Gets data about the submission, depending on the submission type.
        If it is a self post it returns a dictionary of type, title, and text.
        If it is a link to a website, returns a dictionary of type, title, 
        description from the link, text of the link, and keywords of the link.
        Some of these may be None """
        submissionData = {}
        if self.submission.is_self:
            submissionData['type'] = "self"
            submissionData['title'] = self.submission.title.encode('UTF-8')
            submissionData['text'] = self.submission.selftext.encode('UTF-8')
        elif self.submission.url is not None:
            submissionData['type'] = "link"
            title, descr, text, keywords = getPageInfo(self.submission.url)
            if "imgur" in self.submission.url:
                keywords = None
                descr = None
                text = None
                if title is None:
                    title = self.submission.title.encode('UTF-8')
            elif keywords is not None:
                keywords = [keyword.strip() for keyword in keywords.split(",")]
            submissionData['title'] = title
            submissionData['descr'] = descr
            submissionData['text'] = text
            submissionData['keywords'] = keywords
        return submissionData

def test():
    scraper = redditSubmissionScraper('212gl7')
    topSubmissionComments = scraper.getTopSubmissionComments()
    topComments = scraper.getTopComments(topSubmissionComments)
    submissionData = scraper.getSubmittedData()
    for comment in topComments.keys():
        print("----------------------------------------")
        print(comment[0].body)
        for reply in topComments[comment]:
            print('\t' + reply.body)
    print(submissionData)

if __name__ == "__main__":
    test()