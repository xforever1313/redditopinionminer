from nltk.corpus import stopwords
from nltk.tokenize import PunktWordTokenizer
import nltk
import string
import re

class Cleaner():
    """ A class to do various formatting / cleaning / replacing of text """

    def __init__(self):
        self.stopwords = stopwords.words('english')
        self.lemmatizer = nltk.WordNetLemmatizer()
        self.stemmer = nltk.stem.porter.PorterStemmer()

        # This is a regex to parse URLs, courtesy of JohnJohnGa 
        # http://stackoverflow.com/questions/6883049/regex-to-find-urls-in-string-in-python
        self.urlFinder = re.compile("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", re.IGNORECASE)

        # This is a regex to parse email addresses
        # http://www.regular-expressions.info/email.html
        self.emailFinder = re.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$", re.IGNORECASE)

        punctuation = string.punctuation
        specialRegexSymbols = set(['.', '^', '$', '*', '+', '?', '\\', '[', ']', '|', ')', '('])
        punctuationPattern = "(" + "".join([punct + "|" for punct in list(punctuation) if punct not in specialRegexSymbols])
        punctuationPattern += "".join(["\\" + punct + "|" for punct in specialRegexSymbols])[:-1] + ")"
        self.punctuationRegex = re.compile(punctuationPattern)

        # This is a regex to get keywords and keyword phrases
        # http://sunamkim.me/documents/coling2010.pdf
        self.grammar = r"""
        NBAR:
        {<NN.*|JJ>*<NN.*>} # Nouns and Adjectives, terminated with Nouns
        NP:
        {<NBAR>}
        {<NBAR><IN><NBAR>} # Above, connected with in/of/etc...
        """
        self.chunker = nltk.RegexpParser(self.grammar)

        self.emoticons = {}
        self.getEmoticons()
        self.slang = {}
        self.getSlang()

    def normalise(self, word):
        """ Stem and lemmatize words """
        word = word.lower()
        word = self.stemmer.stem_word(word)
        word = self.lemmatizer.lemmatize(word)
        return word

    def removePunctuation(self, word):
        """ Remove punctuation from a given word """
        return self.punctuationRegex.sub("", word)

    def getEmoticons(self):
        """ Emoticons compiled from here: https://en.wikipedia.org/wiki/List_of_emoticons 
        and hand annotated """
        with open('emoticons') as f:
            for line in f:
                line = line.strip().split('\t')
                self.emoticons[line[0]] = line[1]

    def getSlang(self):
        """ Slang compiled from here: http://en.wiktionary.org/wiki/Appendix:Internet_slang
        hand picked most common, annotated with most common definition """
        with open('common_internet_slang') as f:
            for line in f:
                line = line.strip().split('\t')
                self.slang[line[0]] = line[1]

    def tokenize(self, line):
        """ Tokenize a line of text """
        tokens = PunktWordTokenizer().tokenize(line)
        return tokens

    def replaceEmoticons(self, line):
        """ Replace all instances of emoticons in the line """
        for emoticon in self.emoticons.keys():
            #line = re.sub(r"\b" + search + r"\b", self.emoticons.get(emoticon), line, re.UNICODE)
            line = line.replace(emoticon, self.emoticons.get(emoticon))
        return line

    def replaceSlang(self, line):
        """ Replace internet slang / internet abbreviations with their expanded,
        English meanings """
        for slang in self.slang.keys():
            line = re.sub("\\b" + slang + "\\b", self.slang.get(slang), line, re.IGNORECASE)
        return line

    def replaceURLs(self, line):
        """ Replace all instances of URLS with the text httpaddr """
        return self.urlFinder.sub("httpaddr", line)

    def replaceEmail(self, line):
        """ Replace all instances of email addresses with the text emailaddr """
        return self.emailFinder.sub("emailaddr", line)

    def isValidWord(self, word):
        """ Returns true if the word is greater than 2 characters long and less than 40 characters
        and it is not a stopword """
        return (2 <= len(word) <= 40) and (word.lower() not in self.stopwords)

    def getValidWords(self, tokens):
        """ Returns a list of valid words given a list of tentative tokens """
        return [self.normalise(token) for token in tokens if self.isValidWord(token)]

    def getValidTerms(self, tree):
        for leaf in self.leaves(tree):
            term = [self.normalise(w) for w,t in leaf if self.isValidWord(w)]
            yield term

    def posTagTokens(self, tokens):
        """ Tags tokens with their parts of speech """
        return nltk.tag.pos_tag(tokens)


    def leaves(self, tree):
        """ Finds NP (nounphrase) leaf nodes of a chunk tree."""
        for subtree in tree.subtrees(filter = lambda t: t.node=='NP'):
            yield subtree.leaves()

    def getTree(self, posTokens):
        """ Returns a tree of NBAR phrases given tokens that are tagged with parts of speech """
        return self.chunker.parse(posTokens)