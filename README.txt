----------------------------------------------
Reddit Opinion Miner
Authors: Nicolas Schrading and Seth Hendrick
----------------------------------------------
Dependencies required
	NLTK	
		Install:
			sudo pip install -U numpy
			sudo pip install -U pyyaml nltk
		Once Installed:
			python
			>>> import nltk
			>>> nltk.download()
			(You'll need data like English stopwords)

	PRAW
		Install:
			sudo pip install -U praw
	
	Goose
		Install:
			sudo pip install -U goose-extractor

	Scikit-learn
		Install:
			sudo pip install -U scikit-learn

	SciPy
		Install:
			sudo apt-get install python-scipy 

	Matplotlib
		Install:
			sudo apt-get install python-matplotlib

	TextBlob
		Install:
			sudo pip install -U textblob
		Once Installed:
			(If missing some NLTK data)
			python -m textblob.download_corpora
----------------------------------------------
File Structure:
	\redditopinionminer
		opinionMine.py
		redditData.py
		pageScraper.py
		learner.py
		cleaner.py
		textBlobTester.py
		postIDs
		common_internet_slang
		emoticons
		\commentData
			*.done.txt
			allWords.txt
---------------------------------------------	
File Descriptions
	opinionMine.py: 
		This is the main file to run to both gather training data from Reddit
		and run opinion mining on posts given in the postIDs file.
		
		to run, type: python opinionMine.py [-h] [--scrape] postIDs
		The -h option will print help output to the command line
		The --scrape option will make the opinionMiner gather training data
		(comments) from the posts given in the postIDs file
		The postIDs file contains reddit post IDs, 1 per line.

	redditData.py:
		This file contains a class that, given a Reddit submission ID, gathers
		the comments and self-post/external link data of the submission and 
		returns them

	pageScraper.py:
		This file contains functions to utilize Goose to extract external
		webpage text content, including titles, main body, meta keywords,
		and meta descriptions

	learner.py:
		This file contains several classes for training a classifier.
			Comment class - holds state for a Reddit comment, including
			words, features, and label

			DataBuilder class - Takes the *.done.txt files and 
			converts them to an allWords.txt file containing
			all the words in all comments. Converts comments
			to feature vectors. Uses cleaner.py functions to
			clean comment data.

			Learner class - Takes a list of Comment objects.
			Can train on them, perform a validation on 
			different classifiers, test the trained classifiers,
			and perform testing on TextBlob and Scikit functions.

	cleaner.py:
		Contains a class with functions to tokenize, stem, lemmatize, lowercase, 
		get keywords, replace slang, replace emoticons, replace email,
		and otherwise do text-cleaning tasks.

	textBlobTester.py
		Contains functions to get sentiment from textblob.

	postIDs
		Contains a list of Reddit post IDs, 1 per line, to get training
		data from, or perform opinion mining on.

	common_internet_slang
		Contains a list of common internet slang, 1 per line, and 
		their English translations separated by a tab
		All slang is from here: http://en.wiktionary.org/wiki/Appendix:Internet_slang
		Hand picked most common, annotated with most common definition by us

	emoticons
		Contains a list of emoticons, 1 per line, and their
		corresponding sentiment separated by a tab
		All emoticons are from: https://en.wikipedia.org/wiki/List_of_emoticons
		Handpicked, and labeled by us.

	*.done.txt
		Training data with Reddit comments from different postIDs,
		labeled with their sentiments

		sethsNegative, sethsNeutral, and sethsPositive Comments.done.txt
		are hand-picked comments to balance the number of neutral,
		positive, and negative comments to train on

	allWords.txt
		Not initially in this directory, but is built when opinionMine.py is 
		run. Contains all words in the training corpus, sorted by most
		common at the top.
---------------------------------------------
Special Things You Can Do
The learner.py, redditData.py, and pageScraper.py files all have test 
functions that can be run if you run them as main like:
	python learner.py
	python redditData.py
	python pageScraper.py
Running these will simply test the functions in these files and output test output.
In learner.py there are commented out test methods in the test() function. Uncomment
them to run them.
---------------------------------------------
